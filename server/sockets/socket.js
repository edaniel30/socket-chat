const { io } = require('../server');
const { Usuarios } = require('../clases/usuarios');
const { crearMsg } = require('../utilidades/utilidades')

const usuarios = new Usuarios();

io.on('connection', (client) => {

    client.on('entrarChat', (data, callback) => {

        if ( !data.nombre || !data.sala ){
            return callback({
                error: true,
                msg: 'El nombre/sala es necesario'
            })
        }

        client.join(data.sala);

        usuarios.agregarPersona( client.id, data.nombre, data.sala )

        client.broadcast.to(data.sala).emit('listaPersonas', usuarios.obtenerPersonasSala( data.sala ));

        return callback(usuarios.obtenerPersonasSala(data.sala));
    });

    client.on('crearMsg', ( data ) => {

        let persona = usuarios.obtenerPersona( client.id )

        let mensaje = crearMsg( persona.nombre, persona.msg);
        client.broadcast.to(persona.sala).emit('crearMsg', mensaje);
    });

    client.on('disconnect', (data, callback) => {

        let personaBorrada = usuarios.borrarPersona( client.id );
        client.broadcast.to(personaBorrada.sala).emit('crearMsg', crearMsg('Administrador', `${personaBorrada.nombre} salió`));
        client.broadcast.to(personaBorrada.sala).emit('listaPersonas', usuarios.obtenerPersonasSala(personaBorrada.sala));
    });

    // Mensajes privados
    client.on('msgPrivados', data => {

        let persona = usuarios.obtenerPersona( client.id );
        client.broadcast.to(data.para).emit('msgPrivados', crearMsg( persona.nombre, data.msg ));
    });


});